/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hit_api;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author kurnia
 */
public class parse_xml {

    public static void main(String[] args) throws XPathExpressionException {

        String xml = "<?xml version='1.0' encoding='UTF-8'?><response><product_type>PDAM</product_type><mti>0210</mti><pan>074035</pan><processing_code>380000</processing_code><amount>000000132600</amount><transmission_date_time>0104135211</transmission_date_time><stan>000192235147</stan><local_trx_time>135211</local_trx_time><local_trx_date>0104</local_trx_date><settlement_date>0105</settlement_date><merchant_type>6012</merchant_type><acquiring_institution_id>008</acquiring_institution_id><retrieval_ref_no>000192235147</retrieval_ref_no><rc>00</rc><terminal_id>DEVELPLG</terminal_id><acceptor_identification_code>200900100800000</acceptor_identification_code><private_data_48>02160130121 201811201812AGENG GIRIYONO 0202127600 000050002018110000001276000000000000000007-000000002018120000001276000000000000000007-00000000</private_data_48><idpel>02160130121 </idpel><blth>201811201812</blth><name>AGENG GIRIYONO </name><bill_count>02</bill_count><bill_repeat_count>02</bill_repeat_count><rp_tag>127600 </rp_tag><biaya_admin>00005000</biaya_admin><bills><bill><bill_date>201811</bill_date><bill_amount>000000127600</bill_amount><penalty>00000000</penalty><kubikasi>00000007-00000000</kubikasi></bill><bill><bill_date>201812</bill_date><bill_amount>000000127600</bill_amount><penalty>00000000</penalty><kubikasi>00000007-00000000</kubikasi></bill></bills></response>";
//        String xml="<?xml version='1.0' encoding='UTF-8'?><response>><age>35</age><name>aaa</name></customer>";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        InputSource source = new InputSource(new StringReader(xml));
        XPath xpath = XPathFactory.newInstance()
                .newXPath();
        Object response = xpath.evaluate("/response", source, XPathConstants.NODE);
        String product = xpath.evaluate("product_type", response);
        String mti = xpath.evaluate("mti", response);
        String name = xpath.evaluate("name", response);
        String rp = xpath.evaluate("rp_tag", response);
        System.out.println("==============Nested==============");
        String bil = xpath.evaluate("bills/bill/kubikasi", response);
        String date = xpath.evaluate("bills/bill/bill_date", response);
        System.out.println("Produk :"+product + " \nMti :" + mti + "\nname :" + name + "\n Rpl Tag :" + rp + "\n Bill :" + bil + "\n date :" + date);

    }
}
