/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hit_api;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author kurnia
 */
public class koneksi {

     private static Connection koneksi;
    

      private Connection connect;  
   private String driverName = "org.postgresql.Driver"; // Driver Untuk Koneksi Ke PostgreSQL  
   private String jdbc = "jdbc:postgresql://";  
   private String host = "localhost:"; // Host ini Bisa Menggunakan IP Anda, Contoh : 192.168.100.100  
   private String port = "5434/"; // Port Default PostgreSQL  
   private String database = "txn_monitoringa"; // Ini Database yang akan digunakan  
   private String url = jdbc + host + port + database;  
   private String username = "postgres"; //  
   private String password = "kurnia";  
   public Connection getKoneksi() throws SQLException {  
     if (connect == null) {  
       try {  
         Class.forName(driverName);  
         System.out.println("Class Driver Ditemukan");  
         try {  
           connect = DriverManager.getConnection(url, username, password);  
           System.out.println("Koneksi Database Sukses");  
         } catch (SQLException se) {  
           System.out.println("Koneksi Database Gagal : " + se);  
           System.exit(0);  
         }  
       } catch (ClassNotFoundException cnfe) {  
         System.out.println("Class Driver Tidak Ditemukan, Terjadi Kesalahan Pada : " + cnfe);  
         System.exit(0);  
       }  
     }  
     return connect;  
   }  
}
