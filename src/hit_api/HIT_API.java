/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hit_api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.applet.Main;

/**
 *
 * @author kurnia
 */
public class HIT_API {
    
     public static String md5Java(String message) throws NoSuchAlgorithmException {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));
            //merubah byte array ke dalam String Hexadecimal
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return digest;
    }

    static void hitApi() {
                System.out.println("===============================================================");
        
    	String username = "plg12110004";
        String password = "aldwin.3";
        String secret_key = "1234";
        String url = "http://180.211.90.243:8118/Transactions/trx.json";
        String trxType = "2100";
        String trxId = "0";
        String custMsisdn = "212100089886";
        String custAccountNo = "212100089886";
        String prodId = "100";
        Random random = new Random();
        trxId = Integer.toString(random.nextInt(99999999));
              
        try {
            URL obj = new URL(url);
            
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

//            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            // If you use HttpsURLConnection
//            SSLSocketFactory sslSocketFactory = createTrustAllSslSocketFactory();
//            con.setSSLSocketFactory(sslSocketFactory);
//            HostnameVerifier host = createHostName();
//            con.setHostnameVerifier(host);

            con.setConnectTimeout(100000);
            con.setReadTimeout(100000); 
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");	
            String trxDate = sdf.format(new Date());
    		
            String signature = md5Java(username + password + prodId + trxDate + secret_key);

            System.out.println(trxDate);
            System.out.println(signature);
            System.out.println("\nPELANGIREST username="+username+"&password="+password+"&signature="+signature);

            String urlParameters =
                "trx_date="+trxDate+
                "&trx_type="+trxType+
                "&trx_id="+trxId+
                "&cust_msisdn="+custMsisdn+
                "&cust_account_no="+custAccountNo+
                "&product_id="+prodId+
                "&product_nomination="+
                "&periode_payment=";

            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "PELANGIREST username="+username+"&password="+password+"&signature="+signature);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");

            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);
            
            
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();	

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            
            in.close();

            // print result
            System.out.println("aa"+response.toString());
           
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
//    public void insert(String idpel, String request,String response,String request_date,String response_date,String status,String product_id) {
//        String sql = "INSERT INTO product_logs(idpel,request,response,request_date,response_date,status,product_id) VALUES(?,?,?,?,?,?,?,?)";
// 
//        try (Connection conn = this.connect();
//                PreparedStatement pstmt = conn.prepareStatement(sql)) {
//            pstmt.setString(1, idpel);
//            pstmt.setString(2, request);
//            pstmt.setString(3, response);
//            pstmt.setString(4, request_date);
//            pstmt.setString(5, response_date);
//            pstmt.setString(6, status);
//            pstmt.setString(7, product_id);
//            pstmt.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//    }

    public static void main(String[] args) {
        // TODO code application logic here
        hitApi();

    }
}
